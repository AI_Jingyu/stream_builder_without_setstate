import 'dart:async';
import 'package:flutter/material.dart';


/*
counter example without setState() method
*/
class CounterPage extends StatefulWidget {
  @override
  _CounterPageState createState() => _CounterPageState();
}

class _CounterPageState extends State<CounterPage> {

  int _counter=0;
  final StreamController<int> _streamController=StreamController<int>();

  @override
  void dispose() {
    _streamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Stream Version of Counter Page, Counter without setState()'),
      ),
      body: Center(
        child: StreamBuilder<int>(  //listen to the stream, update text when value changed
          stream: _streamController.stream,
          initialData: _counter,
          builder: (BuildContext context, AsyncSnapshot<int> snapshot){
            return Text('You clicked ${snapshot.data} times');
          },
        ),
      ),

      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: (){
          _streamController.sink.add(_counter++);
        },
      ),
    );
  }
}